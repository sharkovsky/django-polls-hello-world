from django.test import TestCase
from polls.models import Poll

class PollTestCase(TestCase):
    def setUp(self):
        Poll.objects.create(question='Question')

    def test_question_exists(self):
        poll = Poll.objects.get(question='Question')
        assert poll.question == 'Question'
